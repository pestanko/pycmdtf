# PyCmdTF: Command Testing Framework

[![pipeline status](https://gitlab.com/pestanko/pycmdtf/badges/master/pipeline.svg)](https://gitlab.com/pestanko/pycmdtf/-/commits/master)

Define suites and scenarios to test your commands. Main of the framework aim is to test executables with asserted exit
code, expected stdout/stderr outputs.

## Installation

From the PyPi registry using pip:

```shell
$ pip install --user pycmdtf
```

### Development version

For development version you will need [``poetry``](https://python-poetry.org/).

```shell
$ git clone git@github.com:pestanko/pycmdtf.git
$ cd pycmdtf

$ poetry install
```

## Usage

If you have do not add ``~/.local/bin`` to `PATH`

```shell
$ python -m pycmdtf.cli 
```

else

```shell
$ pycmdtf 
```

it will produce Help:

```shell
Usage: pycmdtf [OPTIONS] COMMAND [ARGS]...

  Command Test Runtime - Execute test

Options:
  --version             Show the version and exit.
  -L, --log-level TEXT  Set log level
  --help                Show this message and exit.

Commands:
  execute       Execute suite
  help-action   Show help for the action
  list-actions  List all available actions
  parse         Parse suite definition

```

### Example usage:

Execute the suite from examples with debug logging level:

```shell
$ python -m pycmdtf.cli -L debug execute examples/simple/tests/ 
```

## Suites and Scenarios

Take a look at examples for more information.

Each project you would like to test has to have defined the suite (`suite.yml` file).

Each suite should have these parameters:

- `name: (string)` Name of the suite, name has to be unique and contains only `[A-Za-z0-9_]+` characters.
- `desc: (string)` Description of the suite
- `tags: (List[string])` List of tags for the suite

Example of the suite definition:

```yaml
# Suite metadata
suite:
  # name of the suite, must consists of letters, numbers and _ only
  name: example
  # description of the suite, any string
  desc: Example test suite

# Default parameters that will be inherited by each scenario/test
defaults:
  valgrind: true

template_vars:
  # template variables, can be substituted
  # in `defaults` for scenarios, suite or `template` test
  # they can be overridden by '-T' parameter
  # example: -T 'target:solution'
  target: 'source'

# List of scenario files
scenarios:
  - sanity_tests.yml
  - smoke_tests.yml
  - extended_tests.yml
```

### Scenario

Defines multiple tests that should be executed

Each scenario should have these parameters:

- `name: (string)` Name of the scenario, name has to be unique and contains only `[A-Za-z0-9_]+` characters.
- `desc: (string)` Description of the scenario
- `tags: (List[string])` List of tags for the scenario

Example:

```yaml
scenario:
  # Scenario metadata
  name: smoke_tests
  desc: Smoke Tests
  tags:
    - smoke

# Dependencies, list of result scenarios, concrete tests
# or special dependency checks
# they must be executed before this scenario,
# scenario will be executed only if the 
# all dependencies has passed
depends_on:
  - result: sanity_tests # Check result of the sanity_tests
  - file_exists: /usr/bin/echo # check whether file exists 

# Defaults for the tests (its params)
defaults:
  cmd: "echo"
  # "cmd" param will default to "echo" for every test in this scenario

tests:
  - name: echo_hello_world
    desc: Echo hello world
    # all following parameters are test parameters

    # cmd what command will be executed, it can be
    # string or List[string]
    cmd: [ "echo", "hello", "world" ]
    # Exit code (return value) should be 0
    exit: 0
    # STDOUT should have same content as data/cat_word.in
    out: data/cat_word.in
    # STDERR should be empty
    err: empty

  - name: echo_hello_world_2
    desc: Echo hello world with args and cmd params
    cmd: "echo"
    # Args can be provided explicitly
    args: [ "Hello world" ]
    exit: 0
    out: data/hello_world_2.out

  # Templated test with multiple examples:
  - name: case_test
    cmd: "echo"
    # Parameters defined outside template, cannot be parametrized
    # also they would be shared among all cases
    # each case will produce new test
    exit: 0
    # Here comes the templating part
    template:
      desc: "Case: ${case_name}"
      args: [ "-e", "${case_input}" ]
      out:
        # You can directly provide content
        content: "${case_input}\n"
    cases:
      - vars:
          case_name: "Single line"
          case_input: "Hello world"
      - vars:
          case_name: "Two lines"
          case_input: "Hello world\nHow are you"

  - name: explicit_action_and_validators
    desc: Run explicit action and validators
    action:
      # name defines which action will be executed,
      # take a look at action register or use cli to
      # see all available actions
      name: 'cmd'
      # All following parameters are action parameters
      cmd: [ 'echo', 'you can use this' ]
    validators:
      # Yes there is also validator called cmd
      - name: 'cmd'
        exit: 0

```

### Test

Each test should have these parameters:

- `name: (string)` Name of the test, name has to be unique and contains only `[A-Za-z0-9_]+` characters.
- `desc: (string)` Description of the test
- `tags: (List[string])` List of tags for the test
- `no_validate: (bool, default: False)` Do not run any validation on the test
- `required: (bool, default: False)` If the required test fails all other tests of the scenario will not be executed
- `template: (Dict[str, Any], default: None)` Templated parameters, any parameter here that contains
  variable `("${VAR_NAME}")` will be substituted, parameter must be a string
- `cases: (List, default: None)`, each case will produce new test with substituted variables, variables are defined as
  property ``vars`` (take a look at example).

- all other parameters are passed to action and validators
  (despite `action` and `validators`, see advanced parameters).

Advanced parameters:

- `action: (Dict[str, Any])` - explicitly define an action, this should not be necessary, by default `cmd` action is
  used, which action is used is defined by `name` parameter, all other parameters are passed as action parameters
- `validators: (List)` - explicitly define validators, each validator has to have param `name`, to see more information
  take a look at action.

#### Action

Each tests consists of 2 parts, action and action validators, **action** is something that will be executed.
**Action validators** are validating the action result.

##### Example:

Lets take ``cmd`` action, this actions executed the command and stores exit code, stdout and stderr of the command and
passes them as action result to the validators. Stdout and Stderr are stored as files to the ``artifacts`` for the test
that is executed.

There are multiple validators for the cmd command for example the ``cmd`` validator that is validating all possible
parts of the result (Exit code, stdout, stderr). By default ``cmd`` action validated by `cmd` validator validates:

- exit code (`exit` parameter), simple comparison
- stdout, by diffing with expected stdout content
- stderr, by diffing with expected stderr content
- valgrind log by _TODO_

## TODO

- [ ] Valgrind Validation
- [ ] Validate using command
    - How to provide action result to the command?
- [ ] Validate style-check and format
- [ ] Validate binary files
- [ ] Validate any file content (new validator required)
- [x] Templates, cases
- [x] Tags support
- [x] Execute Sub-scenarios
- [x] JUnit report
- [x] Execute commands




