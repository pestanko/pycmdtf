import pytest

from pycmdtf import defs
from pycmdtf.config import Config
from pycmdtf.parser import Parser
from pycmdtf.runner import SuiteRunner, SuiteResult
from tests.helpers import SIMPLE_EXAMPLE


@pytest.fixture()
def cfg() -> Config:
    return Config(SIMPLE_EXAMPLE / 'tests')


@pytest.fixture()
def suite(cfg) -> defs.SuiteDef:
    return Parser(cfg).parse_suite()


@pytest.fixture()
def suite_result(suite: defs.SuiteDef, cfg: Config):
    return SuiteRunner(cfg, suite).run_suite()


def test_execute_definition(suite_result: SuiteResult, suite: defs.SuiteDef):
    assert suite_result.is_pass()
    assert suite_result.df == suite
    assert len(suite_result.scenario_results) == len(suite.scenarios)

    for i, sc_res in enumerate(suite_result.scenario_results):
        assert sc_res.is_pass()
        # All test should be executed
        assert len(sc_res.test_results) == len(suite.scenarios[i].tests)
        # All sub-scenarios should be executed, for simple there are none
        assert len(sc_res.scenario_results) == len(suite.scenarios[i].scenarios)
        for t_res in sc_res.test_results:
            assert t_res.is_pass()
