from pycmdtf import utils


def test_expand_template_string():
    assert utils.expand_template("ahoj", {}) == "ahoj"
    assert utils.expand_template("ahoj", {'world': 'svet'}) == "ahoj"
    assert utils.expand_template("hello ${world}", {}) == "hello ${world}"
    assert utils.expand_template("hello ${world}", {'world': 'svet'}) == "hello svet"


def test_expand_template_list():
    assert utils.expand_template(["ahoj"], {}) == ["ahoj"]
    assert utils.expand_template(["hello", "${world}"], {'world': 'svet'}) == ['hello', 'svet']
    assert utils.expand_template(["hello", "-${world}"], {'world': 'svet'}) == ['hello', '-svet']
