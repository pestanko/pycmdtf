import pytest

from pycmdtf import defs
from pycmdtf.config import Config
from pycmdtf.parser import Parser
from tests.helpers import SIMPLE_EXAMPLE


@pytest.fixture()
def cfg() -> Config:
    return Config(SIMPLE_EXAMPLE / 'tests')


@pytest.fixture()
def suite(cfg) -> defs.SuiteDef:
    return Parser(cfg).parse_suite()


def test_parse_definition(suite: defs.SuiteDef):
    assert suite.name == 'simple'
    assert suite.id == 'simple'
    assert suite.namespace.parts == ['simple']
    assert suite.desc is not None
    assert not suite.tags

    assert len(suite.scenarios) == 3
