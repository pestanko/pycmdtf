#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define BUF_SIZE 1024

int main()
{
    char buffer[BUF_SIZE];

    for(;;) {
        size_t bytes = fread(buffer, sizeof(char), BUF_SIZE, stdin);
        fwrite(buffer, sizeof(char), bytes, stdout);
        if(bytes < BUF_SIZE) {
            break;
        }
    }

    fflush(stdout);

    return 0;
}