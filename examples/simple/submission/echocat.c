#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define BUF_SIZE 1024

int main(int argc, char *argv[])
{
    if(argc == 1)
    {
        printf("Hello world!\n");

        return 0;
    }

    if(strcmp(argv[1], "exit") == 0)
    {
        int exit_code = atoi(argv[2]);
        return exit_code;
    }

    if(strcmp(argv[1], "echo") == 0)
    {
        for(int i = 2; i < argc; i++)
        {
            printf("%s ", argv[i]);
        }

        return 0;
    }

    if(strcmp(argv[1], "cat") == 0)
    {
        char buffer[BUF_SIZE];
        for(;;) {
            size_t bytes = fread(buffer, sizeof(char), BUF_SIZE, stdin);
            fwrite(buffer, sizeof(char), bytes, stdout);
            if(bytes < BUF_SIZE) {
                break;
            }
        }
        fflush(stdout);
    }

    return 0;
}

